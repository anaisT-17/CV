
# CV Developpeur

J'ai réalisé mon CV en format web. 

Celui-ci est composé des sections :

ASIDE :

- Contact
- Expertise
- Formations
- Compétences personnelles 
- Compétences techniques 
- Réseaux sociaux

MAIN :

- Nom
- Profil
- A propos
- Expériences
- Intérêts

Pour cela j'ai utilisé les technologies suivantes :

 - Structure HTML Clean Structure,
 - Commentaires Compréhensibles et concise,
 - Sémantique (balises HTML),
 - Alternatives aux médias (alt),
 - Favicon,
 - W3C Validator,
 - Code Propre (organisation et indentation),
 - Nommage compréhensible et pertinent,
 - Ordonner le CSS par "Zone",
 - Utilisation des Raccourcis en CSS,
 - "Less is More",
 - Google Fonts,
 - Fontawsome
 - 3 WireFrames par page (mobile,tablette,desktop),
 - 0 Frameworks CSS,
 - 0 JS.
 
# Resume Developer

I've realize my Curriculum vitae in web format.
It's composed the sections :

ASIDE :

- Contact
- Expertise
- Formations
- Personal skills
- Technical skills
- Socials networks

MAIN : 

- Name
- Profil
- About
- Experiences
- Interests

For this, I have use the next technologies :

- HTML Clean Structure,
- Comments Comprehensible et concise,
- Sémantic (HTML tags),
- Alternatives to the medias (alt),
- Favicon,
- W3C Validator,
- Code Clean (organization et indentation),
- Comprehensible and relevant naming
- Order CSS by "Zone",
- Using Shortcuts in CSS,
- "Less is More",
- Google Fonts,
- Fontawsome,
- 3 WireFrames par page (mobile,tablet,desktop),
- 0 CSS Frameworks,
- 0 JS.

